/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft_utils_nbr.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.com    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/29 15:03:38 by jrossign          #+#    #+#             */
/*   Updated: 2021/12/09 17:39:37 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_putnbr(int n)
{
	int			length;
	char		*str;
	long long	n2;

	str = ft_itoa(n);
	length = ft_strlen(str);
	n2 = n;
	if (n2 < 0)
	{
		ft_putchar('-');
		n2 *= -1;
	}
	if (n2 > 9)
	{
		ft_putnbr((n2 / 10));
		ft_putchar(n2 % 10 + 48);
	}
	else
		ft_putchar(n2 + 48);
	free(str);
	return (length);
}

int	ft_putnbr_unsigned(unsigned int n)
{
	int		length;
	char	*str;

	str = ft_itoa_unsigned(n);
	length = ft_strlen(str);
	if (n > 9)
	{
		ft_putnbr_unsigned((n / 10));
		ft_putchar(n % 10 + 48);
	}
	else
		ft_putchar(n + 48);
	free(str);
	return (length);
}
