/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/02 16:04:00 by jrossign          #+#    #+#             */
/*   Updated: 2021/12/09 17:11:25 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static char	*string_getter_neg(char *str, long n, int counter)
{
	while (counter > 0)
	{
		str[counter--] = n % 10 + 48;
		n /= 10;
	}
	return (str);
}

static char	*string_getter(char *str, long n, int counter)
{
	while (counter >= 0)
	{
		str[counter--] = n % 10 + 48;
		n /= 10;
	}
	return (str);
}

static int	char_size(long long n)
{
	long	i;
	int		j;

	i = 1;
	j = 0;
	if (n == 0)
		return (1);
	while (n % i != n)
	{
		i *= 10;
		j++;
	}
	return (j);
}

char	*ft_itoa(int n)
{
	long	nb;
	char	*str;

	nb = n;
	if (n >= 0)
		str = ft_calloc(char_size(nb) + 1, sizeof(char));
	else
	{
		str = ft_calloc(char_size(nb) + 2, sizeof(char));
		if (!str)
			return (NULL);
		str[0] = '-';
		return (string_getter_neg(str, nb * -1, char_size(nb)));
	}
	if (!str)
		return (NULL);
	return (string_getter(str, nb, char_size(nb) - 1));
}

char	*ft_itoa_unsigned(unsigned int n)
{
	long	nb;
	char	*str;

	nb = n;
	if (n >= 0)
		str = ft_calloc(char_size(nb) + 1, sizeof(char));
	else
	{
		str = ft_calloc(char_size(nb) + 2, sizeof(char));
		if (!str)
			return (NULL);
		str[0] = '-';
		return (string_getter_neg(str, nb * -1, char_size(nb)));
	}
	if (!str)
		return (NULL);
	return (string_getter(str, nb, char_size(nb) - 1));
}
