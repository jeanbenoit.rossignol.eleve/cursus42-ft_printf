/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/24 15:26:46 by jrossign          #+#    #+#             */
/*   Updated: 2021/12/10 12:19:51 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <stdarg.h>
# include <stdlib.h>
# include <stddef.h>
# include <unistd.h>

//ft_printf.c
int			ft_printf(const char *c, ...);
int			ft_printf_router(char c, va_list list);
//printf_char.c
int			ft_utils_char(char arg, va_list list);
//printf_nbr.c
int			ft_utils_nbr(char arg, va_list list);
//printf_hex.c
int			ft_utils_hex(char arg, va_list list);
//printf_percent
int			ft_utils_percent(char arg);
//printf_ptr.c
int			ft_utils_ptr(char arg, va_list list);
//libft_utils_char.c
int			ft_putchar(char c);
int			ft_putstr(char *str);
size_t		ft_strlen(const char *str);
//libft_utils_nbr.c
int			ft_putnbr(int n);
int			ft_putnbr_unsigned(unsigned int n);
//libft_utils_hex.c
int			ft_puthex_router(char c, unsigned int val);
int			ft_puthex(unsigned int val, char *hex_string);
int			hex_length(unsigned int val);
//libft_utils_ptr.c
int			ft_putptr(unsigned long ptr);
int			ft_printfptr(unsigned long val);
int			ptr_length(unsigned long val);
//ft_itoa.c
char		*ft_itoa(int n);
char		*ft_itoa_unsigned(unsigned int n);
//ft_calloc
void		*ft_calloc(size_t count, size_t size);

#endif
