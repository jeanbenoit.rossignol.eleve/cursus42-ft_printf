/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft_utils_hex.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/06 15:26:22 by jrossign          #+#    #+#             */
/*   Updated: 2021/12/10 12:19:05 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_puthex_router(char c, unsigned int val)
{
	char	*hex_lower;
	char	*hex_upper;

	hex_lower = "0123456789abcdef";
	hex_upper = "0123456789ABCDEF";
	if (val == 0)
		return (ft_putchar('0'));
	else if (c == 'x')
		return (ft_puthex(val, hex_lower));
	else
		return (ft_puthex(val, hex_upper));
}

int	ft_puthex(unsigned int val, char *hex_string)
{
	long long	nb2;
	int			char_counter;

	nb2 = val;
	char_counter = hex_length(val);
	if (val > 0)
	{
		ft_puthex((val / 16), hex_string);
		ft_putchar(hex_string[val % 16]);
	}
	return (char_counter);
}

int	hex_length(unsigned int val)
{
	int	counter;

	counter = 0;
	while (val > 0)
	{
		val /= 16;
		counter++;
	}
	return (counter);
}
