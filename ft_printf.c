/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/24 15:22:49 by jrossign          #+#    #+#             */
/*   Updated: 2021/12/09 17:28:25 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_printf(const char *c, ...)
{
	va_list	list;
	int		i;
	int		char_counter;

	va_start(list, c);
	i = 0;
	char_counter = 0;
	while (c[i])
	{
		if (c[i] == '%')
		{
			char_counter += ft_printf_router(c[i + 1], list);
			i++;
		}
		else
			char_counter += ft_putchar(c[i]);
		i++;
	}
	return (char_counter);
}

int	ft_printf_router(char c, va_list list)
{
	int	char_counter;

	char_counter = 0;
	if (c == 'c' || c == 's')
		char_counter = ft_utils_char(c, list);
	else if (c == 'i' || c == 'd' || c == 'u')
		char_counter = ft_utils_nbr(c, list);
	else if (c == 'x' || c == 'X')
		char_counter = ft_utils_hex(c, list);
	else if (c == '%')
		char_counter = ft_utils_percent(c);
	else if (c == 'p')
	{
		char_counter = ft_utils_ptr(c, list);
	}
	return (char_counter);
}
