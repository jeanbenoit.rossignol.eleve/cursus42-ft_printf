/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printf_hex.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/06 15:26:34 by jrossign          #+#    #+#             */
/*   Updated: 2021/12/10 11:48:01 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_utils_hex(char arg, va_list list)
{
	int	char_counter;

	char_counter = 0;
	char_counter = ft_puthex_router(arg, va_arg(list, unsigned int));
	return (char_counter);
}
