/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/02 16:09:54 by jrossign          #+#    #+#             */
/*   Updated: 2021/12/09 17:11:15 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	*ft_calloc(size_t count, size_t size)
{
	char			*c;
	size_t			malloc_size;
	unsigned int	i;

	malloc_size = count * size;
	c = malloc(malloc_size);
	i = 0;
	if (c == NULL)
		return (NULL);
	while (i < malloc_size)
	{
		c[i] = 0;
		i++;
	}
	return ((void *)c);
}
