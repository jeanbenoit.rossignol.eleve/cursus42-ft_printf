/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printf_nbr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/29 14:54:53 by jrossign          #+#    #+#             */
/*   Updated: 2021/12/09 17:13:21 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_utils_nbr(char arg, va_list list)
{
	int	char_counter;

	char_counter = 0;
	if (arg == 'i')
		char_counter = ft_putnbr(va_arg(list, int));
	if (arg == 'd')
		char_counter = ft_putnbr(va_arg(list, int));
	if (arg == 'u')
		char_counter = ft_putnbr_unsigned(va_arg(list, int));
	return (char_counter);
}
