/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printf_ptr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/08 13:53:36 by jrossign          #+#    #+#             */
/*   Updated: 2021/12/09 17:13:42 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_utils_ptr(char arg, va_list list)
{
	unsigned long	ptr_ad;
	int				char_counter;

	char_counter = 0;
	if (arg == 'p')
	{
		ptr_ad = va_arg(list, unsigned long);
		char_counter = ft_putptr(ptr_ad);
	}
	return (char_counter);
}
