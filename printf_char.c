/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printf_char.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/29 14:46:18 by jrossign          #+#    #+#             */
/*   Updated: 2021/12/09 17:12:53 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_utils_char(char arg, va_list list)
{
	int	char_counter;

	char_counter = 0;
	if (arg == 'c')
		char_counter = ft_putchar(va_arg(list, int));
	if (arg == 's')
		char_counter = ft_putstr(va_arg(list, char *));
	return (char_counter);
}
