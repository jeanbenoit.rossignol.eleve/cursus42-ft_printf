/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printf_percent.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/06 20:31:42 by jrossign          #+#    #+#             */
/*   Updated: 2021/12/09 17:13:32 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_utils_percent(char arg)
{
	int	char_counter;

	char_counter = 0;
	if (arg == '%')
		char_counter = ft_putchar('%');
	return (char_counter);
}
