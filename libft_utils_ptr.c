/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft_utils_ptr.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/08 13:53:13 by jrossign          #+#    #+#             */
/*   Updated: 2021/12/10 11:59:28 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	ft_putptr(unsigned long ptr)
{
	unsigned long	char_counter;

	char_counter = 0;
	char_counter += ft_putchar('0');
	char_counter += ft_putchar('x');
	if (ptr == 0)
		char_counter += ft_putchar('0');
	else
		char_counter += ft_printfptr(ptr);
	return (char_counter);
}

int	ft_printfptr(unsigned long val)
{
	char	*hex;
	int		char_counter;

	hex = "0123456789abcdef";
	char_counter = ptr_length(val);
	if (val > 0)
	{
		ft_printfptr((val / 16));
		ft_putchar(hex[val % 16]);
	}
	return (char_counter);
}

int	ptr_length(unsigned long val)
{
	int	counter;

	counter = 0;
	while (val > 0)
	{
		val /= 16;
		counter++;
	}
	return (counter);
}
