SRCS_M = ft_calloc.c ft_itoa.c ft_printf.c libft_utils_char.c libft_utils_hex.c \
		 libft_utils_nbr.c libft_utils_ptr.c printf_char.c \
		 printf_hex.c printf_nbr.c printf_percent.c printf_ptr.c

OBJS_M = ${SRCS_M:.c=.o}

NAME = libftprintf.a

CC = gcc

AR = ar

ARFLAG = rcs

CFLAGS = -Wall -Werror -Wextra

%.o:		%.c
				${CC} ${CFLAGS} -o $@ -c $^

${NAME}:	${OBJS_M}
				${AR} ${ARFLAG} $@ $^

all:		${NAME}

clean:
			rm -f ${OBJS_M}

fclean: clean
			rm -f ${NAME}

re: fclean all

.PHONY: all clean fclean re
